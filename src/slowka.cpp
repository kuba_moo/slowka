#include <QtGui>
#include <QtXml>

#include "slowka.h"
#include "words.h"
#include "core.h"
#include "popuptranslation.h"

Slowka::Slowka(Core *parent, Words *wordsStore)
    : core(parent), words(wordsStore), ui(new Ui::Slowka)
{
        mini = new Mini(&slowa1, &slowa2);
	mini->uloz();
	stats = new Stats(&slowa1, &slowa2);

        generator = new SoundGenerator(this);

        ui->setupUi(this);

        createActions();

        ui->waitLab->setVisible(false);

        QIcon mainIco(":/main");
        tray = new QSystemTrayIcon(mainIco,this);
        QMenu *trayMenu = new QMenu();
        trayMenu->addAction(core->getAction(Core::Add));
        trayMenu->addAction(core->getAction(Core::Pause));
        trayMenu->addAction(core->getAction(Core::Remove));
        trayMenu->addSeparator();
        trayMenu->addAction(ui->miniAct);
        trayMenu->addAction(ui->quitAct);
        tray->setContextMenu(trayMenu);
        tray->hide();

        ui->column1Lab->setPalette(mini->jeden);
        ui->column1Lab->setForegroundRole(QPalette::Text);
        ui->column2Lab->setPalette(mini->dwa);
        ui->column2Lab->setForegroundRole(QPalette::Text);

        connect(tray,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
                SLOT(showMain(QSystemTrayIcon::ActivationReason)));
	
        connect(ui->words1Tex->verticalScrollBar(),SIGNAL(sliderMoved(int)),
                ui->words2Tex->verticalScrollBar(),SLOT(setValue(int)));
        connect(ui->words2Tex->verticalScrollBar(),SIGNAL(sliderMoved(int)),
                ui->words1Tex->verticalScrollBar(),SLOT(setValue(int)));
	
        connect(ui->defaultBut, SIGNAL(clicked()), SLOT(defaults()));
        connect(ui->font1But,  SIGNAL(clicked()), SLOT(font1()));
        connect(ui->font2But,  SIGNAL(clicked()), SLOT(font2()));
        connect(ui->color1But, SIGNAL(clicked()), SLOT(kolor1()));
        connect(ui->color2But, SIGNAL(clicked()), SLOT(kolor2()));
        connect(ui->miniGrp, SIGNAL(toggled(bool)), SLOT(miniShow(bool)));
        connect(ui->trayChk, SIGNAL(stateChanged(int)),SLOT(trayOpt(int)));
	
        connect(core->getAction(Core::Pause), SIGNAL(toggled(bool)), SLOT(pauza(bool)));
        connect(ui->srcGrp, SIGNAL(clicked()), core->getAction(Core::Pause), SLOT(toggle()));
	/* End GUI Functions */

	clipboard = QApplication::clipboard();	
	
	connect(clipboard, SIGNAL(selectionChanged()),	SLOT(newWord()));
	connect(clipboard, SIGNAL(dataChanged()), 	SLOT(newDataWord()));

        connect(mini,SIGNAL(zamknieto()), ui->miniAct, SLOT(trigger()));
        connect(ui->words1Tex,SIGNAL(textChanged()), SLOT(lista1Changed()));
        connect(ui->words2Tex,SIGNAL(textChanged()), SLOT(lista2Changed()));

        connect(ui->generate, SIGNAL(clicked()), SLOT(synth()));
        connect(generator, SIGNAL(progressChanged(int)),
                ui->generateProg, SLOT(setValue(int)));
        connect(generator, SIGNAL(downloadCompleted()), SLOT(synthCompleted()));

	restoreAll();

    connect(core, SIGNAL(popupAccepted(QString,QStringList)), SLOT(popupAccepted(QString,QStringList)));

    /* Save settings every 5 minutes in case of a crash. */
    QTimer *timer = new QTimer(this);
    timer->start(5 * 60 * 1000);
    connect(timer, SIGNAL(timeout()), SLOT(saveAll()));
}

Slowka::~Slowka() {
}

void Slowka::popupAccepted(QString w, QStringList t)
{
    while (slowa1.size() < slowa2.size())
        slowa1 << "";

    while (slowa1.size() > slowa2.size())
        slowa2 << "";

    slowo(w);
    slowo(t.join(", "));
}

void Slowka::pauza(bool a) {
        ui->srcGrp->setChecked(! a);
}

void Slowka::createActions() {
        connect(ui->switchAct, SIGNAL(triggered()), SLOT(switchLists()));
        connect(ui->openAct, SIGNAL(triggered()),SLOT(openList()));
        connect(ui->openRecentAct, SIGNAL(triggered()),SLOT(openRecentList()));
        connect(ui->saveAct, SIGNAL(triggered()),SLOT(saveList()));
        connect(ui->saveAsAct, SIGNAL(triggered()),SLOT(saveAsList()));
        connect(ui->printAct, SIGNAL(triggered()),SLOT(printList()));
        connect(ui->statsAct, SIGNAL(triggered()),SLOT(showStats()));
        connect(ui->generateAct, SIGNAL(triggered()),SLOT(generujListe()));
        connect(ui->applyAct, SIGNAL(triggered()),SLOT(apply()));
        connect(ui->truncateAct, SIGNAL(triggered()),SLOT(truncate()));
        connect(ui->closeAct, SIGNAL(triggered()),SLOT(closeList()));

        connect(core->getAction(Core::Remove), SIGNAL(triggered()), SLOT(delLast()));
        connect(mini->delBut, SIGNAL(clicked()), core->getAction(Core::Remove), SLOT(trigger()));
        connect(mini->pauseBut, SIGNAL(toggled(bool)), core->getAction(Core::Pause), SLOT(setChecked(bool)));
        connect(core->getAction(Core::Pause), SIGNAL(toggled(bool)), mini->pauseBut, SLOT(setChecked(bool)));

        connect(mini->newBut, SIGNAL(clicked()), core->getAction(Core::Add), SLOT(trigger()));

        connect(ui->quitAct,SIGNAL(triggered()),SLOT(zamknij()));
        connect(ui->miniAct,SIGNAL(triggered(bool)),SLOT(miniShow(bool)));

        connect(ui->parleyConnect,SIGNAL(toggled(bool)),core,SLOT(parleyConnect(bool)));
}

void Slowka::restoreAll() {
  QSettings settings;

  settings.beginGroup("Main");
  switch(settings.value("ToolbarArea", Qt::LeftToolBarArea).toInt()) {
  case 1:
    addToolBar(Qt::LeftToolBarArea, ui->toolBar);
    break;
  case 2:
    addToolBar(Qt::RightToolBarArea, ui->toolBar);
    break;
  case 4:
    addToolBar(Qt::TopToolBarArea, ui->toolBar);
    break;
  case 8:
    addToolBar(Qt::BottomToolBarArea, ui->toolBar);
    break;
  }
  if(settings.value("ShowOnTray", false).toBool())
    ui->trayChk->click();
  if (! settings.value("EnableSources", true).toBool())
      ui->srcGrp->setChecked(true);
  if(!settings.value("GrabMarking", true).toBool())
    ui->markChk->click();
  if(!settings.value("GrabCopying", true).toBool())
    ui->copyChk->click();
  ui->popupTranslations->setChecked(settings.value("ShowTranslations", true).toBool());
  resize(settings.value("Size", QSize(340, 450)).toSize());
  move(settings.value("Position", QPoint(350, 160)).toPoint());
  QList<int> lista;
  lista << settings.value("Splitter1", 150).toInt();
  lista << settings.value("Splitter2", 150).toInt(); 
  ui->wordsSplit->setSizes(lista);
  settings.endGroup();

  settings.beginGroup("Mini");
  QColor tmpColor = settings.value("Color1").value<QColor>();
  mini->jeden.setColor(QPalette::Text, tmpColor);
  ui->column1Lab->setPalette(mini->jeden);
  mini->kolor1();
  tmpColor = settings.value("Color2").value<QColor>();
  mini->dwa.setColor(QPalette::Text, tmpColor);
  ui->column2Lab->setPalette(mini->dwa);
  mini->kolor2();
  ui->miniGrp->setChecked(settings.value("ShowMini", false).toBool());
  QFont tmpFont = settings.value("Font1").value<QFont>();
  font1(&tmpFont);
  tmpFont = settings.value("Font2").value<QFont>();
  font2(&tmpFont);
  mini->resize(settings.value("MiniSize", QSize(440, 29)).toSize());
  mini->move(settings.value("MiniPosition", QPoint(300, 130)).toPoint());
  mini->uloz();
  settings.endGroup();

  settings.beginGroup("Session");
  switch(settings.value("SessionRestore", 0).toInt()) {
  case 0:
    ui->clearSesRad->click();
    break;
  case 1:
    ui->fileSesRad->click();
    break;
  case 2:
    ui->fullSesRad->click();
    break;
  }
  recentFileName = settings.value("Filename").toString();
  if( ui->fullSesRad->isChecked() ) {
    fileName = recentFileName;
    slowa1 = settings.value("Words1").toStringList();
    slowa2 = settings.value("Words2").toStringList();
    generujSlowka();
    ui->listTex->insertPlainText(settings.value("List").toString());
  }
  settings.endGroup();
  
  settings.beginGroup("Synthesier");
  ui->synthDir->setText(settings.value("CacheDir", core->getWorkingDirectory())
                        .toString());
  ui->nextSpin->setValue(settings.value("NextInterval", 1.0).toDouble());
  ui->translationSpin->setValue(settings.value("TranslationInterval", 0.6).toDouble());
  settings.endGroup();

  if( ui->fileSesRad->isChecked() && !recentFileName.isEmpty())
    openRecentList();

  ui->parleyConnect->setChecked(settings.value("parleyConnect", false).toBool());

  return;
}

void Slowka::saveAll() {
  QSettings settings;

  settings.beginGroup("Main");
  settings.setValue("ToolbarArea", toolBarArea(ui->toolBar));
  settings.setValue("ShowOnTray", ui->trayChk->isChecked());
  settings.setValue("EnableSources", ui->srcGrp->isChecked());
  settings.setValue("GrabMarking", ui->markChk->isChecked());
  settings.setValue("GrabCopying", ui->copyChk->isChecked());
  settings.setValue("ShowTranslations", ui->popupTranslations->isChecked());
  settings.setValue("Size", size());
  settings.setValue("Position", pos());
  settings.setValue("Splitter1", ui->wordsSplit->sizes().at(0));
  settings.setValue("Splitter2", ui->wordsSplit->sizes().at(1));
  settings.endGroup();

  settings.beginGroup("Mini");
  settings.setValue("Color1", mini->jeden.color(QPalette::Text));
  settings.setValue("Color2", mini->dwa.color(QPalette::Text));
  settings.setValue("ShowMini", ui->miniGrp->isChecked());
  settings.setValue("Font1", ui->column1Lab->font());
  settings.setValue("Font2", ui->column2Lab->font());
  settings.setValue("MiniSize", mini->size());
  settings.setValue("MiniPosition", mini->pos());
  settings.endGroup();
  
  settings.beginGroup("Session");
  if( ui->clearSesRad->isChecked() )
    settings.setValue("SessionRestore", 0);
  if( ui->fileSesRad->isChecked() )
    settings.setValue("SessionRestore", 1);
  if( ui->fullSesRad->isChecked() )
    settings.setValue("SessionRestore", 2);
  settings.setValue("Filename", fileName);
  if (ui->fullSesRad->isChecked()) {
    settings.setValue("Words1", slowa1);
    settings.setValue("Words2", slowa2);
    settings.setValue("List", ui->listTex->toPlainText());
  }
  settings.endGroup();

  settings.beginGroup("Synthesier");
  settings.setValue("CacheDir", ui->synthDir->text());
  settings.setValue("NextInterval", ui->nextSpin->value());
  settings.setValue("TranslationInterval", ui->translationSpin->value());
  settings.endGroup();

  settings.setValue("parleyConnect", ui->parleyConnect->isChecked());
}

void Slowka::defaults() {
  this->setGeometry(350,160,340,450);

  mini->setGeometry(300,130,440,29);

  ui->trayChk->setChecked(false);
  ui->clearSesRad->setChecked(true);
  ui->srcGrp->setChecked(true);
  ui->copyChk->setChecked(true);
  ui->markChk->setChecked(true);
  ui->popupTranslations->setChecked(true);
  ui->miniGrp->setChecked(false);
  QFont font("Sans Serif", 12);
  font1( &font );
  font2( &font );
  mini->jeden.setColor(QPalette::Text, QColor(155,0,0));
  ui->column1Lab->setPalette(mini->jeden);
  mini->kolor1();
  mini->dwa.setColor(QPalette::Text, QColor(0,155,0));
  ui->column2Lab->setPalette(mini->dwa);
  mini->kolor2();
  mini->uloz();
  QList<int> lista;
  lista << 150 << 150;
  ui->wordsSplit->setSizes(lista);

  addToolBar(Qt::LeftToolBarArea, ui->toolBar);

  ui->synthDir->setText(core->getWorkingDirectory());
  ui->nextSpin->setValue(1.0);
  ui->translationSpin->setValue(0.6);
}

void Slowka::zamknij() {
	saveAll();
	qApp->quit();
}

void Slowka::delLast() {
	if(slowa1.size() > slowa2.size()) {
		if(slowa1.size() > 0) 
			slowa1.removeLast();
	} else {
		if(slowa2.size() > 0)
			slowa2.removeLast();
	}
	
	generujSlowka();
}

void Slowka::closeEvent(QCloseEvent *event) {
        if(ui->trayChk->isChecked()) {
		this->hide();
		event->ignore();
	} else {
		saveAll();
		mini->close();
		event->accept();	
	}
	
}

void Slowka::miniShow(bool a) {
	if(a) {
		mini->show();
                ui->miniGrp->setChecked(true);
                ui->miniAct->setChecked(true);
	} else {
                ui->miniGrp->setChecked(false);
                ui->miniAct->setChecked(false);
		mini->hide();
	}
}

void Slowka::trayOpt(int a) {
	if(a)
		tray->show();
	else
		tray->hide();
}

void Slowka::showMain(QSystemTrayIcon::ActivationReason a) {
	if( a == QSystemTrayIcon::DoubleClick ) {
		if(this->isHidden()) 
			this->show();
		else
			this->hide();
	}
}

void Slowka::_newWord(bool add) {
    QString word = clipboard->text(QClipboard::Selection).simplified().toLower();

    if (add)
        slowo(word);

    if (ui->popupTranslations->isChecked())
        core->popupTranslation();
}

void Slowka::newWord() {
    if (core->getAction(Core::Pause)->isChecked() || this->isActiveWindow())
        return;

    _newWord(ui->markChk->isChecked());
}

void Slowka::newDataWord() {
    if (core->getAction(Core::Pause)->isChecked() || this->isActiveWindow())
        return;

    _newWord(ui->copyChk->isChecked());
}

void Slowka::slowo(QString a) {
	if(a.isEmpty())
		return;

        if(slowa1.size() <= slowa2.size())
		slowa1 << a;
        else
                slowa2 << a;
	
	generujSlowka();
}

void Slowka::generujSlowka() {
        ui->words1Tex->blockSignals(true);
        ui->words1Tex->clear();
	for(int i=0; i < slowa1.size(); i++)
                ui->words1Tex->append(slowa1.at(i));
        ui->words1Tex->blockSignals(false);
	
        ui->words2Tex->blockSignals(true);
        ui->words2Tex->clear();
	for(int i=0; i < slowa2.size(); i++)
                ui->words2Tex->append(slowa2.at(i));
        ui->words2Tex->blockSignals(false);
	
	mini->uloz();
}

void Slowka::lista1Changed() {
        if(ui->words1Tex->isActiveWindow()) {
                slowa1 = ui->words1Tex->toPlainText().split("\n");
	
		mini->uloz();
	}
}

void Slowka::lista2Changed() {
        if( ui->words2Tex->isActiveWindow() ) {
                slowa2 = ui->words2Tex->toPlainText().split("\n");
	
		mini->uloz();
	}
}

void Slowka::generujListe() {
        ui->listTex->clear();
	for (int i=0; (i<slowa1.size())||(i<slowa1.size()); i++) {
		if(i<slowa1.size())
                        ui->listTex->insertPlainText(slowa1.at(i).trimmed());
		if(i<slowa2.size()) {
                        ui->listTex->insertPlainText("\t\t- ");
                        ui->listTex->insertPlainText(slowa2.at(i).trimmed());
		}
		if(i+1<slowa1.size())
                        ui->listTex->insertPlainText("\n");
	}
}

void Slowka::openList(QString* string) {
	QString a;
	
	if( string == 0 ) {
		if( fileName.isEmpty() )
                        a = QFileDialog::getOpenFileName(this, trUtf8("Otwórz"),
                              QString(),
                              trUtf8("All Files (*);;KDE Word List (*.kvtml)"));
		else
                        a = QFileDialog::getOpenFileName(this, trUtf8("Otwórz"),
                              fileName,
                              trUtf8("All Files (*);;KDE Word List (*.kvtml)"));
	} else
		a = *string;
		
	if( a.isEmpty() )
		return;
	
	if( !fileName.isEmpty() )
		recentFileName = fileName;
	fileName = a;
	
	if( !QFile::exists(a) )
		return;
	QFile file(a);
	if( !file.open(QFile::ReadOnly) )
		return;
	
	if( a.right(5) != "kvtml" ) {
		QByteArray data = file.readAll();
		QTextCodec *codec = Qt::codecForHtml(data);
		QString str = codec->toUnicode(data);
		if (Qt::mightBeRichText(str)) {
                        ui->listTex->setHtml(str);
		} else {
			str = QString::fromLocal8Bit(data);
                        ui->listTex->setPlainText(str);
		}
		
		apply();
	} else {
		QXmlStreamReader reader(&file);
		slowa1.clear();
		slowa2.clear();

		while( !reader.atEnd() ) {
			reader.readNext();
		
			if( reader.isStartElement() ) {
				if( reader.name() == "kvtml" )
					if( reader.attributes().value( "version" ) == "2.0" ) {
						QString translation = "", transId = "0";
						while( !reader.atEnd() ) {
							reader.readNext();
						
							if( reader.isStartElement() ) {
								if( reader.name() == "translation" )
									transId = reader.attributes().value( "id" ).toString();
								if( reader.name() == "text" ) {
									if( transId == "0" ) {
										if( !translation.isEmpty() ) {
											slowa2 << translation.trimmed();
											translation = "";
										}
										slowa1 << reader.readElementText();
									} else {
										translation += " " + reader.readElementText();
									}
								}
							}
								
							if( reader.hasError() )
								QMessageBox::warning(this, trUtf8("Słówka"), trUtf8("Błąd podczas czytania pliku %1 (linia %2): %3").arg(a).arg(reader.lineNumber()).arg(reader.errorString()), QMessageBox::Ok);
						}
						if( !translation.isEmpty() ) {
							slowa2 << translation.trimmed();
							translation = "";
						}
					}
				if( reader.name() == "o" ) 
					slowa1 << reader.readElementText();
				if( reader.name() == "t" ) 
					slowa2 << reader.readElementText();
			}
			
			if( reader.hasError() )
				QMessageBox::warning(this, trUtf8("Słówka"), trUtf8("Błąd podczas czytania pliku %1 (linia %2): %3").arg(a).arg(reader.lineNumber()).arg(reader.errorString()), QMessageBox::Ok);
		}
		generujListe();
		generujSlowka();
	}
}	

void Slowka::apply() {
	slowa1.clear();
	slowa2.clear();
        QStringList pom = ui->listTex->toPlainText().split("\n");
	for(int i = 0; i < pom.size(); i++) {
		if( pom.at(i).trimmed().split("-").size() >= 1 )
			slowa1 << pom.at(i).trimmed().split("- ").at(0).simplified();
		if( pom.at(i).trimmed().split("-").size() >= 2 )
			slowa2 << pom.at(i).trimmed().split("- ").at(1).simplified();
	}
	
	generujSlowka();
}

void Slowka::truncate() {
	slowa1.clear();
	slowa2.clear();
        ui->listTex->clear();
	generujSlowka();
}

void Slowka::switchLists() {
	QStringList pom = slowa1;
	slowa1 = slowa2;
	slowa2 = pom;

	generujSlowka();
}

void Slowka::openRecentList() {
	if( !recentFileName.isEmpty() )
		openList(&recentFileName);
	else
		openList();
}

void Slowka::saveAsList(QString* str) {
	QString zapis;
	if( str == 0 ) {
		if( fileName.isEmpty() )
                        zapis = QFileDialog::getSaveFileName(this, tr("Zapisz"),
                                     QString(),
                                     tr("All Files (*);;KDE Word List (*.kvtml)"));
		else
                        zapis = QFileDialog::getSaveFileName(this, tr("Zapisz"),
                                     fileName,
                                     tr("All Files (*);;KDE Word List (*.kvtml)"));
		
	} else
		zapis = *str;
		
	if( zapis.isEmpty() )
		return;
	else
		fileName = zapis;

	QFile file(zapis);
	if (!file.open(QFile::WriteOnly))
		return;
	QTextStream ts(&file);
	ts.setCodec(QTextCodec::codecForName("UTF-8"));
	if( zapis.right(5) != "kvtml" )
                ts << ui->listTex->document()->toPlainText();
	else {
		QXmlStreamWriter writer(&file);
		writer.setAutoFormatting(true);
		writer.writeStartDocument();

		writer.writeStartElement("kvtml");

		QStringList pom1, pom2;
		pom1 = slowa1;
		pom2 = slowa2;
		while( pom1.size() > 0 || pom2.size() > 0 ) {
			writer.writeStartElement("e");
			if( pom1.size() > 0 ) {
				writer.writeStartElement("o");
				writer.writeCharacters(pom1.at(0).trimmed());
				writer.writeEndElement(); // o
				pom1.removeFirst();
			}
			if( pom2.size() > 0 ) {
				writer.writeStartElement("t");
				writer.writeCharacters(pom2.at(0).trimmed());
				writer.writeEndElement(); // t
				pom2.removeFirst();
			}	
			writer.writeEndElement(); // e
		}

		writer.writeEndElement(); // kvtml

		writer.writeEndDocument();
	}
}

void Slowka::saveList() {
	if( fileName.isEmpty() )
		saveAsList();
	else
		saveAsList(&fileName);
}

void Slowka::printList() {
     QTextDocument *document = ui->listTex->document();
	QPrinter printer;

	QPrintDialog *dlg = new QPrintDialog(&printer, this);
	if (dlg->exec() != QDialog::Accepted)
		return;

	document->print(&printer);
}

void Slowka::closeList() {	
	recentFileName = fileName;
	fileName = "";
	
        ui->words1Tex->clear();
	slowa1.clear();
        ui->words2Tex->clear();
	slowa2.clear();
        ui->listTex->clear();
	mini->uloz();
}

void Slowka::font1( QFont *font ) {
	bool ok(1);
	if( font == 0 )
                font = new QFont( QFontDialog::getFont(&ok, ui->column1Lab->font()) );
	if( ok ) {
                ui->column1Lab->setFont(*font);
		mini->font1(font);
	}
}

void Slowka::font2(QFont *font ) {
	bool ok(1);
	if( font == 0 )
                font = new QFont( QFontDialog::getFont(&ok, ui->column2Lab->font()) );
	if( ok ) {
                ui->column2Lab->setFont(*font);
		mini->font2(font);
	}
}

void Slowka::kolor1() { 
	QColor color(QColorDialog::getColor(mini->jeden.color(QPalette::Text)));
	if( color.isValid() ) {
		mini->jeden.setColor(QPalette::Text, color);
                ui->column1Lab->setPalette(mini->jeden);
		mini->kolor1();
	}
}

void Slowka::kolor2() {
	QColor color(QColorDialog::getColor(mini->dwa.color(QPalette::Text)));
	if( color.isValid() ) {
		mini->dwa.setColor(QPalette::Text, color);
                ui->column2Lab->setPalette(mini->dwa);
		mini->kolor2();
	}
}

void Slowka::showStats() {
	stats->setGeometry( x()+width()/2-80, y()+height()/2-60, 160, 110 );
	stats->updateStats();
	stats->show();
}

void Slowka::synth()
{
    if (generator->getCacheDir() != ui->synthDir->text())
        generator->setCacheDir(ui->synthDir->text());

    generator->setSilence(ui->nextSpin->value(), ui->translationSpin->value());
    generator->synth(slowa1, 9, "gb", slowa2, 2, "pl");

    ui->generate->setVisible(false);
    ui->waitLab->setVisible(true);
}

void Slowka::synthCompleted()
{
    ui->generate->setVisible(true);
    ui->waitLab->setVisible(false);
}
