#include "soundgenerator.h"

#include "ivonaloader.h"

#include <QDebug>
#include <QDir>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlResult>
#include <QSqlRecord>
#include <QVariant>
#include <QFileDialog>
#include <QSqlError>

void IvonaThread::run()
{
    loader = new IvonaLoader(2);
    inited = true;

    exec();
}

SoundGenerator::SoundGenerator(QObject *parent) :
    QObject(parent), cacheDirPath()
{
    isWorking = false;

    ivona = new IvonaThread();
    ivona->start();

    database = QSqlDatabase::addDatabase("QSQLITE");
}

SoundGenerator::~SoundGenerator()
{
    database.commit();
    database.close();

    // TODO: clean up thread
}

void SoundGenerator::setCacheDir(QString cacheDir)
{
    cacheDirPath = cacheDir;

    if (database.isOpen())
        database.close();

    QDir dir(cacheDirPath);
    if (! dir.exists())
        dir.mkpath(cacheDirPath);

    database.setDatabaseName(cacheDirPath + "/database");
    if (database.open())
        database.exec("CREATE TABLE IF NOT EXISTS list("
                                    "word VARCHAR(250) PRIMARY KEY ASC,"
                                    "voice INT,"
                                    "language CHAR(2)"
                      ")");
}

void SoundGenerator::setSilence(double next, double translation)
{
    silenceTranslation = translation * 10;
    silenceNext = next * 10;
}

void SoundGenerator::synth(QStringList first, int vfirst, QString lfirst,
                           QStringList second, int vsecond, QString lsecond)
{
    if (! database.isOpen())
    {
        QMessageBox::critical(0, trUtf8("Pliki tymczasowe"),
                              trUtf8("Brak dostępu do plików w katalogu plików "
                                     "tymczasowych.\n Upewnij się, że pole ma porawną"
                                     "wartość i spróbuj ponownie."), QMessageBox::Ok);
        return;
    }

    if (isWorking)
        return;

    qDebug() << "SoundGenerator: started synth";
    isWorking = true;
    isFailed = false;
    list1 = first;
    list2 = second;
    voice1 = vfirst;
    voice2 = vsecond;
    lang1 = lfirst;
    lang2 = lsecond;
    todoCount = doneCount = 0;

    QString str;
    QString queryStr = QString("SELECT * FROM list WHERE word = '%1'")
            + QString("AND voice = %1 AND language = '%2'").arg(voice1).arg(lang1);
    foreach (str, first)
    {
        QSqlQuery exists(queryStr.arg(str.replace('\'', "\\'")));
        if (! exists.next())
        {
            todoCount++;
            enqueueSynth(str, voice1, lang1);
        }
    }
    queryStr = QString("SELECT * FROM list WHERE word = '%1'")
            + QString("AND voice = %1 AND language = '%2'").arg(voice2).arg(lang2);
    foreach (str, second)
    {
        QSqlQuery exists(queryStr.arg(str.replace('\'', "\\'")));
        if (! exists.next())
        {
            todoCount++;
            enqueueSynth(str, voice2, lang2);
        }
    }

    if (todoCount)
        emit progressChanged(0);
    else {
        emit downloadCompleted();
        generate();
    }
}

void SoundGenerator::pieceReady()
{
    QPair<SampleDesc*, QByteArray> piece = ivona->loader->getResult();

    QSqlQuery q = database.exec(QString("INSERT INTO list(word, voice, language)"
                          "VALUES('%1', %2, '%3')")
                                .arg(piece.first->text.replace('\'', "\\'"))
                                .arg(piece.first->voice).arg(piece.first->lang));
    QVariant id = q.lastInsertId();
    if (! id.isValid())
        qDebug() << "SoundGenerator: Inserting data failed" << q.lastError().text();
    else
    {
        QFile file(cacheDirPath + '/' + QString("%1").arg(id.toInt()));
        file.open(QIODevice::WriteOnly);
        file.write(piece.second);
        file.close();
    }

    delete piece.first;

    if (todoCount != ++doneCount)
    {
        emit progressChanged(100*doneCount/todoCount);
    }
    else
    {
        emit progressChanged(100);
        emit downloadCompleted();
        if (! isFailed)
            generate();
        else
            QMessageBox::critical(0, trUtf8("Pobieranie plików"),
                                  trUtf8("Nie udało się pobrać wszystkich plików,\n"
                                         "spróbuj ponownie za chwilę\n"),
                                  QMessageBox::Cancel);
    }
}

void SoundGenerator::downloadFailed()
{
    isFailed = true;
    doneCount++;
}

void SoundGenerator::generate()
{
    isWorking = false;

    QFileDialog dialog;
    dialog.setWindowTitle(trUtf8("Zapisz nagranie słówek"));
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(tr("Pliki mp3 (*.mp3)"));
    if (dialog.exec() == QDialog::Rejected)
        return;

    QFile mp3(dialog.selectedFiles().at(0));
    if (! mp3.open(QIODevice::WriteOnly))
    {
        QMessageBox::critical(0, trUtf8("Zapis pliku"),
                              trUtf8("Nie można wykonać zapisu do podanego pliku:\n")
                                     + mp3.errorString(), QMessageBox::Cancel);
        return;
    }

    QFile silenceFile(":/silence");
    if (! silenceFile.open(QIODevice::ReadOnly))
    {
        mp3.close();
        QMessageBox::critical(0, trUtf8("Zapis pliku"),
                              trUtf8("Nie można odnaleźć pliku ciszy, prawdopodobnie"
                                     "instalacja programu została uszkodzona."),
                                     QMessageBox::Cancel);
        return;
    }

    QByteArray silence = silenceFile.readAll();
    QByteArray translation = silence.repeated(silenceTranslation);
    QByteArray next = silence.repeated(silenceNext);

    QList<QString>::iterator it1 = list1.begin(), it2 = list2.begin();

    while (it1 != list1.end() && it2 != list2.end())
    {
        QSqlQuery q1(QString("SELECT rowid FROM list WHERE word = '%1' "
                     "AND voice = %2 AND language = '%3'")
                     .arg((*it1).replace('\'', "\\'")).arg(voice1).arg(lang1));
        if (! q1.first())
            qDebug() << "SoundGenerator: getting piece id from database failed";

        int fieldNo = q1.record().indexOf("rowid");
        QString id = q1.value(fieldNo).toString();
        QVariant var = q1.value(fieldNo);
        QFile sample1(cacheDirPath + QString("/%1").arg(id));
        if (! sample1.open(QIODevice::ReadOnly))
            qDebug() << "SoundGenerator: opening sample file failed"
                     << sample1.errorString();
        mp3.write(sample1.readAll());
        sample1.close();

        mp3.write(translation);

        QSqlQuery q2(QString("SELECT rowid FROM list WHERE word = '%1' "
                     "AND voice = %2 AND language = '%3'")
                     .arg((*it2).replace('\'', "\\'")).arg(voice2).arg(lang2));
        if (! q2.first())
            qDebug() << "SoundGenerator: getting piece id from database failed";

        id = q2.value(fieldNo).toString();
        QFile sample2(cacheDirPath + '/' + QString("%1").arg(id));
        if (! sample2.open(QIODevice::ReadOnly))
            qDebug() << "SoundGenerator: opening sample file failed"
                     << sample2.errorString();
        mp3.write(sample2.readAll());
        sample2.close();

        mp3.write(next);

        it1++;
        it2++;
    }

    mp3.close();
}

void SoundGenerator::enqueueSynth(QString text, int voice, QString lang)
{
    while (! ivona->inited);

    if (! ivona->connected) {
        connect(ivona->loader, SIGNAL(downloadCompleted()), SLOT(pieceReady()));
        connect(ivona->loader, SIGNAL(downloadFailed()),
             this, SLOT(downloadFailed()));
        ivona->connected = true;
    }

    ivona->loader->enqueueSynth(new SampleDesc(text, voice, lang));
}
