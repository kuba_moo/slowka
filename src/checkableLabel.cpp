#include "checkableLabel.h"

#include <QDebug>

CheckableLabel::CheckableLabel(QWidget *parent, Qt::WindowFlags f) :
    QLabel(parent, f)
{
}

CheckableLabel::CheckableLabel(const QString & text, QWidget *parent, Qt::WindowFlags f) :
    QLabel(text, parent, f)
{
}

bool CheckableLabel::isChecked() const
{
    return frameShape() == QFrame::Box;
}

void CheckableLabel::setTextAdjust(const QString& text)
{
    setText(text);
    adjustSize();
}

void CheckableLabel::mousePressEvent(QMouseEvent *ev)
{
    setFrameShape(static_cast<QFrame::Shape>
                  (frameShape() ^ QFrame::Box));

    QLabel::mousePressEvent(ev);
}
