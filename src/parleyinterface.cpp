#include "parleyinterface.h"

#include <QtDBus/QtDBus>

ParleyInterface::ParleyInterface(QObject *parent) :
    QObject(parent)
{
}

void ParleyInterface::add(QString w, QStringList t)
{
    QStringList arg;
    arg << w << t.join(", ");

    QString name = findParleyProcName();
    if (!name.length())
        return;
    QDBusMessage m = QDBusMessage::createMethodCall(name,
                                                   "/AddWithTranslation",
                                                   "local.Editor.EditorWindow",
                                                   "addWordWithTranslation");
    m << arg;

    QDBusMessage response = QDBusConnection::sessionBus().call(m);
    if (response.type() == QDBusMessage::ErrorMessage)
        qDebug() << "Failed to contact parley";
}

QString ParleyInterface::findParleyProcName()
{
    QDBusConnection bus = QDBusConnection::sessionBus();

    QDBusInterface dbus_iface("org.freedesktop.DBus", "/org/freedesktop/DBus",
                              "org.freedesktop.DBus", bus);
    QStringList names = dbus_iface.call("ListNames").arguments().at(0).toStringList();
    QString name;
    foreach (name, names)
        if (name.contains("parley"))
            return name;

    return QString();
}
