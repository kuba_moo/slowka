#ifndef SOUNDGENERATOR_H
#define SOUNDGENERATOR_H

#include <QObject>
#include <QThread>
#include <QSqlDatabase>
#include <QStringList>

class IvonaLoader;

class IvonaThread : public QThread
{
    Q_OBJECT
public:
    explicit IvonaThread() : QThread(), inited(false), connected(false) {}
    void run();

    IvonaLoader *loader;
    volatile bool inited, connected;
};

class SoundGenerator : public QObject
{
    Q_OBJECT
public:
    explicit SoundGenerator(QObject *parent = 0);
    ~SoundGenerator();

    QString getCacheDir() { return cacheDirPath; }
    void setCacheDir(QString);
    void setSilence(double, double);
    void synth(QStringList, int, QString, QStringList, int, QString);

signals:
    void progressChanged(int);
    void downloadCompleted();

public slots:

private slots:
    void pieceReady();
    void downloadFailed();

private:
    void generate();
    void enqueueSynth(QString text, int voice, QString lang);

    QSqlDatabase database;
    IvonaThread *ivona;

    bool isWorking;
    bool isFailed;       /* Had an error in current downloads */

    QString cacheDirPath;
    int silenceTranslation, silenceNext;

    int todoCount, doneCount;

    QStringList list1, list2;
    int voice1, voice2;
    QString lang1, lang2;
};

#endif // SOUNDGENERATOR_H
