#ifndef MINI_H
#define MINI_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QPoint>
#include <QColor>

class Mini : public QWidget {
	Q_OBJECT

	public:
		Mini(QStringList*, QStringList* ,QWidget *parent = 0);
		~Mini();
		
		QPalette jeden, dwa;
		QPushButton *pauseBut, *delBut, *newBut;
		void uloz();
		void font1( QFont* );
		void font2( QFont* );
		void kolor1();
		void kolor2();
		
	signals:
		void zamknieto();
	
        protected:
                void mouseMoveEvent(QMouseEvent*);
		void mousePressEvent(QMouseEvent*);
		void mouseReleaseEvent(QMouseEvent*);		
		void wheelEvent(QWheelEvent*);
		void closeEvent(QCloseEvent*); 
		
	private:
		bool pressed;
		QStringList *slo1, *slo2;
		QLabel *label[2][15];
		QPoint start;
}; 
#endif
