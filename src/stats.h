#ifndef STATS_H
#define STATS_H

#include <QWidget>
#include <QLabel>

class Stats : public QWidget {
	QStringList* list1, * list2;
	QLabel* one, *two;

public:
	Stats( QStringList*, QStringList* );

	void updateStats();
};

#endif

