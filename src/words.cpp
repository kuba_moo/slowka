#include "words.h"

Words::Words(QObject *parent) :
    QObject(parent)
{
}

Words::~Words()
{
    clear();
}

Words::InsertionStatus Words::addWord(QString w)
{
    if (wordsSet.contains(w)) {
        return Duplicate;
    }

    Word *word = new Word();
    word->word = w;

    wordsSet.insert(w);
    entries << word;
    wordsList << w;
    emit wordsChanged();

    return Added;
}

Words::InsertionStatus Words::addTranslation(QString t)
{
    if (entries.isEmpty()) {
        return NoEntry;
    }

    QStringList& translations = entries.last()->translations;

    if (translations.contains(t)) {
        return Duplicate;
    }

    translations << t;
    if (translations.size() == 1) {
        translationsList << t;
    } else {
        translationsList.last() += "; " + t;
    }
    emit translationsChanged();

    return Added;
}

Words::InsertionStatus Words::add(QString w, QStringList t)
{
    if (wordsSet.contains(w)) {
        return Duplicate;
    }

    while (wordsSet.size() > translationsList.size())
        translationsList.append(" ");

    addWord(w);
    return addTranslation(t.join(", "));
}

void Words::clear()
{
    Word *w;
    foreach (w, entries) {
        delete w;
    }

    entries.clear();
    wordsSet.clear();

    emit wordsChanged();
    emit translationsChanged();
}

const QStringList& Words::getWords()
{
    return wordsList;
}

const QStringList& Words::getTranslations()
{
    return translationsList;
}

Words::InsertionStatus Words::append(QString s)
{
    if (!entries.isEmpty() && entries.last()->translations.isEmpty()) {
        return addTranslation(s);
    }

    return addWord(s);
}

void Words::removeLast()
{
    if (entries.isEmpty()) {
        return;
    }

    /* Try to remove translation first. */
    QStringList& translations = entries.last()->translations;
    if (!translations.isEmpty()) {
        translations.removeLast();

        QStringList tempLast = translationsList.last().split("; ");
        tempLast.removeLast();
        translationsList.last() = tempLast.join("; ");

        emit translationsChanged();

        return;
    }

    /* No translation in last entry - remove the word. */
    QString lastWord = entries.last()->word;
    wordsSet.remove(lastWord);

    delete entries.last();
    entries.removeLast();
    wordsList.removeLast();
    emit wordsChanged();

    return;
}
