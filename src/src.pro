SOURCES += slowka.cpp \
           main.cpp \
 mini.cpp \
 stats.cpp \
    ivonaloader.cpp \
    soundgenerator.cpp \
    popuptranslation.cpp \
    application.cpp \
    words.cpp \
    core.cpp \
    checkableLabel.cpp \
    parleyinterface.cpp
HEADERS += slowka.h \
 mini.h \
 stats.h \
    ivonaloader.h \
    soundgenerator.h \
    popuptranslation.h \
    application.h \
    words.h \
    core.h \
    checkableLabel.h \
    parleyinterface.h
TEMPLATE = app
CONFIG += warn_on \
	  thread \
          qt
TARGET = ../bin/slowka
RESOURCES = application.qrc
QT += xml \
        network \
        sql \
        script \
        dbus

FORMS += \
    slowka.ui \
    popupTranslation.ui

unix:LIBS += -lX11

OTHER_FILES += \
    TODO.txt
