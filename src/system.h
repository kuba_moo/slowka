#ifndef SYSTEM_H
#define SYSTEM_H

#include <QObject>

class System : public QObject
{
    Q_OBJECT
public:
    static System& instance()
    {
        static System instance;
        return instance;
    }

    void registerShortcut(quint32 key, quint32 modifiers);

signals:

public slots:

private:
    System(QObject *parent = 0);
    System(const System& ) : QObject(parent()) {}
    System& operator=(const System& ) { return *this; }
};

#endif // SYSTEM_H
