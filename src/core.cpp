#include "core.h"
#include "application.h"
#include "words.h"
#include "parleyinterface.h"
#include "popuptranslation.h"

#include <QClipboard>
#include <QDesktopWidget>
#include <QDir>

Core::Core(Application *parent, Words* wordsStore) :
    QObject(parent), application(parent), words(wordsStore),  parley(0)
{
    input = new QInputDialog();
    /* Closing dialog now prevents it from exiting application
     * when cancel is hit. */
    input->close();

    /* Move input to the center of the screen. */
    QDesktopWidget desktop;
    input->move(desktop.availableGeometry().center());

    setupActions();
}

Core::~Core()
{
    delete input;
}

QString Core::getWorkingDirectory() const
{
    return QDir::homePath() + "/.slowka";
}

QAction* Core::getAction(Action a)
{
    return actions[a];
}

void Core::manualAdd()
{
    input->setWindowTitle(tr("Add item"));
    input->setLabelText(tr("Item to add:"));
    input->setTextValue("");

    int ret = input->exec();
    if (ret) {
        words->append(input->textValue());
    }
}

void Core::manualAddWord()
{
    input->setWindowTitle(tr("Add word"));
    input->setLabelText(tr("Word to add:"));
    input->setTextValue("");

    int ret = input->exec();
    if (ret) {
        words->addWord(input->textValue());
    }
}

void Core::manualAddTranslation()
{
    input->setWindowTitle(tr("Add translation"));
    input->setLabelText(tr("Translation to add:"));
    input->setTextValue("");

    int ret = input->exec();
    if (ret) {
        words->addTranslation(input->textValue());
    }
}

void Core::popupTranslation()
{
    QString word = QApplication::clipboard()->text(QClipboard::Selection)
                                              .simplified().toLower();

    if (PopupTranslation::getActiveInstances())
        return;

    PopupTranslation *popup = new PopupTranslation(word);
    connect(popup, SIGNAL(accepted(QString,QStringList)), words, SLOT(add(QString,QStringList)));
    if (parley)
        connect(popup, SIGNAL(accepted(QString,QStringList)), parley, SLOT(add(QString,QStringList)));
    connect(popup, SIGNAL(accepted(QString,QStringList)), SIGNAL(popupAccepted(QString,QStringList)));
    popup->show();
}

void Core::parleyConnect(bool state)
{
    if (state && !parley) {
        parley = new ParleyInterface(this);
        return;
    }

    if (!parley)
        return;

    parley->deleteLater();
    parley = 0;
}

void Core::setupActions()
{
    /* FIXME: Meta doesn't work, 'cause it has same mask as NumLock. */
    actions[Pause] = new QAction(QIcon(":/pause"), tr("Stop auto-adding"), this);
    actions[Pause]->setCheckable(true);
    actions[Pause]->setShortcut(tr("Shift+Ctrl+Z"));
    actions[Pause]->setShortcutContext(Qt::ApplicationShortcut);
    application->registerGlobalShortcut(actions[Pause]);

    actions[Remove] = new QAction(QIcon(":/minus"), tr("Remove last"), this);
    actions[Remove]->setShortcut(tr("Ctrl+R"));
    actions[Pause]->setShortcutContext(Qt::ApplicationShortcut);
    connect(actions[Remove], SIGNAL(triggered()), words, SLOT(removeLast()));

    actions[Add] = new QAction(QIcon(":/plus"), tr("Add"), this);
    actions[Add]->setShortcut(tr("Ctrl+A"));
    actions[Pause]->setShortcutContext(Qt::ApplicationShortcut);
    connect(actions[Add], SIGNAL(triggered()), SLOT(manualAdd()));

    actions[AddWord] = new QAction(QIcon(":/plus-w"), tr("Add word"), this);
    actions[AddWord]->setShortcut(tr("Meta+Z"));
    actions[Pause]->setShortcutContext(Qt::ApplicationShortcut);
    connect(actions[AddWord], SIGNAL(triggered()), SLOT(manualAddWord()));

    actions[AddTranslation] = new QAction(QIcon(":/plus-t"), tr("Add translation"), this);
    actions[AddTranslation]->setShortcut(tr("Meta+X"));
    actions[Pause]->setShortcutContext(Qt::ApplicationShortcut);
    connect(actions[AddTranslation], SIGNAL(triggered()), SLOT(manualAddTranslation()));

    actions[Translate] = new QAction(tr("Translate"), this);
    actions[Translate]->setShortcut(tr("Ctrl+Shift+X"));
    actions[Translate]->setShortcutContext(Qt::ApplicationShortcut);
    application->registerGlobalShortcut(actions[Translate]);
    connect(actions[Translate], SIGNAL(triggered()), SLOT(popupTranslation()));
}
