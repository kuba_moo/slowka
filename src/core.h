#ifndef CORE_H
#define CORE_H

#include <QAction>
#include <QInputDialog>
#include <QObject>
#include <QProcess>
#include <QString>
#include <QWidget>

class Application;
class Words;
class ParleyInterface;

/** Core object of application.
  *
  * Core object is responsible for whole application logic excluding GUI
  * but including shortcuts, configuration etc.
  */
class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(Application *parent, Words *wordsStore);
    ~Core();

    /* Returns full path to directory where app should store its files. */
    QString getWorkingDirectory() const;

    /** Core actions */
    enum Action {
        Pause = 0,
        Remove,
        Add,
        AddWord,
        AddTranslation,
        Translate,
        _NumActions
    };

    /* Returns one of the core actions. */
    QAction *getAction(Action a);

signals:
    /* Hack to notify main window about word added from popup. */
    void popupAccepted(QString, QStringList);

public slots:
    /* Displays an input dialog and appends the inserted word. */
    void manualAdd();
    void manualAddWord();
    void manualAddTranslation();
    /* Displays popup translation of word in clipboard. */
    void popupTranslation();
    /* Controls Parley interface. */
    void parleyConnect(bool);

private:
    void setupActions();
    QAction *actions[_NumActions];

private:
    Application *application;
    Words *words;
    QInputDialog *input;
    ParleyInterface *parley;
};

#endif // CORE_H
