#include "popuptranslation.h"
#include "ui_popupTranslation.h"

#include <QHBoxLayout>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSpacerItem>
#include <QtScript/QScriptEngine>
#include <QDebug>

unsigned int PopupTranslation::instances = 0;

PopupTranslation::PopupTranslation(QString word) :
    QFrame(0, Qt::Popup),
    ui(new Ui::PopupTranslation),
    target(word), modalityReset(false)
{
    ui->setupUi(this);
    instances++;

    QString url = "https://translate.google.com/translate_a/single?client=t&sl=en&tl=pl&hl=en&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&otf=1&ssel=0&tsel=0&kc=4&tk=522872|570605&q=" + word;
    //QString url = "https://translate.google.com/translate_a/t?client=t&text=" + word + "&hl=en&sl=en&tl=pl&multires=1&otf=1&ssel=4&tsel=0&notlr=0&sc=1";

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QNetworkRequest request(url);
    connect(manager, SIGNAL(finished(QNetworkReply*)), SLOT(response(QNetworkReply*)));
    manager->get(request);

    url = "https://www.google.pl/search?q=" + word + "&hl=en;gws_rd=cr&amp;ei=xZ-0VZW0EIGPsgH71oWgBw";
    QNetworkRequest request_cnt(url);
    manager->get(request_cnt);

    setAttribute(Qt::WA_DeleteOnClose);

    ui->target->setText("Loading translation of <b>" + word + "</b>...");
    ui->target->adjustSize();

    setGeometry(QCursor::pos().x(), QCursor::pos().y(),
                ui->target->width(), ui->target->height());

    this->startTimer(3000);

    connect(ui->add, SIGNAL(clicked()), SLOT(add()));
}

PopupTranslation::~PopupTranslation()
{
    instances--;
}

void PopupTranslation::add()
{
    QStringList translationList;
    CheckableLabel *label;

    foreach(label, translations) {
        if (!label->isChecked())
            continue;

        QString translation = label->text();
        if (translation.contains('.'))
            translation.remove(0, translation.lastIndexOf('.') + 2);

        translationList << translation.toLower().simplified();
    }

    if (!translationList.isEmpty())
        emit accepted(target, translationList);
}

void PopupTranslation::timerEvent(QTimerEvent *)
{
    if (modalityReset)
        return;

    qDebug() << "WARNING: Popup stuck for more then 3 sec, regaining modality";

    hide();
    setWindowModality(Qt::ApplicationModal);
    show();

    modalityReset = true;

    return;
}

void PopupTranslation::response(QNetworkReply *reply)
{
    if (reply->request().url().toString().startsWith("https://translate.google.com/"))
        responseTranslation(reply);
    else if (reply->request().url().toString().startsWith("https://www.google.pl/search?q="))
        responseCount(reply);
    else
        qDebug() << "WARNING: unknown response URL [" << reply->request().url().toString() << "]";
}

void PopupTranslation::responseCount(QNetworkReply *reply)
{
    QByteArray site = reply->readAll();
    QString str(site);
    QRegExp regex("About ([0-9,.]+) results");

    regex.indexIn(str);

    QString popularity;
    int len = regex.cap(1).length();

    if (len >= 11) /* >= 100M */
        popularity = "<b><font color=\"#006600\">basic </font></b>";
    else if (len >= 10) /* >= 10M */
        popularity = "<b><font color=\"#22DD22\">common </font></b>";
    else if (len >= 9) /* >= 1M */
        popularity = "<b><font color=\"#2FAD89\">normal </font></b>";
    else if (len >= 7) /* >= 100k (comma takes on place) */
        popularity = "<b><font color=\"#3399CC\">rare </font></b>";
    else
        popularity = "<b><font color=\"#FF0000\">singular </font></b>";

    ui->footer->setText(popularity + "<i><font color=\"grey\" size=\"2\">" + regex.cap(0) + "</font></i>");
    ui->footer->adjustSize();
}

void PopupTranslation::responseTranslation(QNetworkReply *reply)
{
    /* We receive JSON, have to evaluate it. */
    QScriptEngine engine;
    QScriptValue whole = engine.evaluate(reply->readAll());

    if (whole.isArray())
    {
        QScriptValue translation = whole.property(0).property(0).property(0);

        ui->target->setText(target + " -");
        ui->target->adjustSize();
        ui->header->setTextAdjust(trUtf8(translation.toString().toAscii()));
        translations.append(ui->header);

        QScriptValue dictionary = whole.property(1);

        QStringList items;
        for (int i=0; dictionary.property(i).isValid(); i++)
        {
            QVBoxLayout *columnLayout = new QVBoxLayout();
            QScriptValue column = dictionary.property(i);

            QString str, text;

            QLabel *lab = new QLabel("<b>"+column.property(0).toString()+"</b>", this);
            columnLayout->addWidget(lab);

            int j=1;
            items.clear();
            qScriptValueToSequence(column.property(1), items);
            foreach(str, items) {
                text = QString(" %1. %2").arg(j++)
                        .arg(trUtf8(str.toAscii()));

                CheckableLabel *lab = new CheckableLabel(text, this);
                lab->adjustSize();
                translations.append(lab);
                columnLayout->addWidget(lab);
            }

            columnLayout->addStretch();
            ui->columns->addLayout(columnLayout);
        }
    } else {
        ui->target->setText(trUtf8(QString("Translation of <b>" + target +
                                       "</b> not found").toAscii()));
        ui->target->adjustSize();
    }

    adjustSize();
}
