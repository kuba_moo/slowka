#ifndef CHECKABLELABEL_H
#define CHECKABLELABEL_H

#include <QLabel>

class CheckableLabel : public QLabel
{
public:
    CheckableLabel( QWidget * parent = 0, Qt::WindowFlags f = 0 );
    CheckableLabel( const QString & text, QWidget * parent = 0, Qt::WindowFlags f = 0 );

    bool isChecked() const;
    void setTextAdjust(const QString &text);

protected:
    virtual void mousePressEvent(QMouseEvent * ev);
};

#endif // CHECKABLELABEL_H
