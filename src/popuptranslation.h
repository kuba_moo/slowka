#ifndef POPUPTRANSLATION_H
#define POPUPTRANSLATION_H

#include <QLabel>
#include <QFrame>
#include <QNetworkReply>

/** PopupTranslation - popup window automatically loading translation
  *
  * Class represents a yellow popup box displaying translation
  * of currently selected text. Translation is automatically loaded
  * from the Internet. Window disappears following behaviour of popups.
  *
  * There is a bug causing popup to sometimes loose modality making it
  * impossible to close translation, as a workaround after translation
  * is displayed for 3 seconds, timer event causes reset of popup modality.
  */

namespace Ui {
    class PopupTranslation;
}

class CheckableLabel;

class PopupTranslation : public QFrame
{
    Q_OBJECT

public:
    /* Create new instance */
    explicit PopupTranslation(QString word);
    ~PopupTranslation();

    /* Returns number of active instances, should always be < 2 */
    static unsigned int getActiveInstances()
        { return instances; }

signals:
    void accepted(QString, QStringList);

public slots:

protected:
    void timerEvent(QTimerEvent *);

private slots:
    void response(QNetworkReply*);
    void responseTranslation(QNetworkReply*);
    void responseCount(QNetworkReply*);

    void add();

private:
    static unsigned int instances;
    Ui::PopupTranslation *ui;
    QString target;
    QVector<CheckableLabel *> translations;

    bool modalityReset;
};

#endif // POPUPTRANSLATION_H
