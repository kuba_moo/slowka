#include <QTranslator>

#include "application.h"
#include "core.h"
#include "words.h"
#include "slowka.h"

int main( int argc, char *argv[] ) {
  Q_INIT_RESOURCE(application);

  Application app(argc, argv);

  QCoreApplication::setOrganizationName("moorray");
  QCoreApplication::setOrganizationDomain("moorray.net");
  QCoreApplication::setApplicationName(QApplication::tr("Slowka"));

  Words *words = new Words(&app);
  Core *core = new Core(&app, words);

  Slowka * mw = new Slowka(core, words);
  mw->show();

  return app.exec();
}
