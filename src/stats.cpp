#include <QtGui>

#include "stats.h"

Stats::Stats( QStringList* a, QStringList* b ) {
	setWindowTitle(tr("Statystyka"));
	setWindowIcon(QIcon(":/stats"));
	list1 = a;
	list2 = b;

	one = new QLabel(this);
	one->setGeometry( 10, 20, 200, 30);
	two = new QLabel(this);
	two->setGeometry( 10, 50, 200, 30);
}

void Stats::updateStats() {
	one->setText(QString(trUtf8("Liczba słówek: %1")).arg(list1->size()));
	int powt = 0;
	for( int i=0; i < list1->size(); i++ ) {
		if( list1->count( list1->at(i) ) > 1 )
			powt++;
	}
	two->setText(QString(trUtf8("Liczba powtórek: %1")).arg(powt));
}
