#ifndef IVONALOADER_H
#define IVONALOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QByteArray>
#include <QMap>
#include <QNetworkCookie>
#include <QQueue>
#include <QTime>
#include <QMutex>

struct SampleDesc
{
    QString text;
    int voice;
    QString lang;

    SampleDesc(QString t, int v, QString l) : text(t), voice(v), lang(l) {}

    bool operator ==(const SampleDesc& o)
    {
        return text == o.text && voice == o.voice && lang == o.lang;
    }
};

struct ManagerDesc
{
    static const unsigned maxRequests = 40;
    static const unsigned maxRetry = 10;

    enum State {
        NotConnected,
        SessionReady,
        GeneratingLink,
        DownloadingMp3
    };

     State state;
     QTime timeStart;
     unsigned useCount;
     unsigned retryCount;
     SampleDesc* sample;

     explicit ManagerDesc() : useCount(0) {}

     void startRequest(State s)
     {
         state = s;
         timeStart = QTime::currentTime();
         useCount++;
         retryCount = 0;
     }

     bool isExpired()
     {
         return retryCount == maxRequests;
     }
};

class IvonaLoader : public QObject
{
    Q_OBJECT
public:

    explicit IvonaLoader(int numManagers, QObject *parent = 0);

    QPair<SampleDesc*, QByteArray> getResult();

signals:
    void workAdded();
    void downloadFailed();
    void downloadCompleted();

public slots:
    void enqueueSynth(SampleDesc *);

private slots:
    void tryToWork();
    void initializeManager(QNetworkReply*);
    void generateLink(QNetworkReply*);
    void downloadMp3(QNetworkReply*);

private:
    void spawnAccessManager();

    int numManagers;

    QQueue<SampleDesc*> work;
    QMutex workLock;

    QQueue<QNetworkAccessManager*> managersReady;
    QList<QNetworkAccessManager*> managersWorking;
    QMap<QNetworkAccessManager*, ManagerDesc> managersDesc;

    QQueue<QPair<SampleDesc*, QByteArray> > results;
    QMutex resultsLock;
};

#endif // IVONALOADER_H
