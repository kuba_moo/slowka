#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QAction>
#include <QMap>

/** Reimplementation of QApplication
  *
  * Allows us to register global shortcuts.
  */
class Application : public QApplication
{
    Q_OBJECT
public:
    explicit Application(int& argc, char** argv);

    virtual bool x11EventFilter(XEvent *ev);

    /* Register global shortcut to action. Use action's shortcut as default. */
    void registerGlobalShortcut(QAction* action);
    void registerGlobalShortcut(QAction* action, QKeySequence shortcut);

signals:

public slots:

private:
    QMap<QKeySequence, QAction*> globalShortcuts;
};

#endif // APPLICATION_H
