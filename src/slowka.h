#ifndef SLOWKA_H
#define SLOWKA_H

#include <QFileDialog>
#include <QMainWindow>
#include <QSettings>
#include <QStringList>
#include <QSystemTrayIcon>
#include <QTextEdit>
#include <QToolBar>
#include <QWidget>

#include "mini.h"
#include "stats.h"
#include "ui_slowka.h"
#include "soundgenerator.h"

class Core;
class Words;

class Slowka:public QMainWindow
{
      Q_OBJECT

public:
      Slowka(Core *parent, Words *wordsStore);
      ~Slowka();

protected:
	void createActions();
    void restoreAll();
	void closeEvent(QCloseEvent*);

protected slots:
    void saveAll();

private slots:
	void miniShow(bool);
	void trayOpt(int);
	void showMain(QSystemTrayIcon::ActivationReason);
	
        void pauza(bool);
	void delLast();
	void newWord();
	void newDataWord();
	void slowo(QString);
	void openList(QString* = 0);
	void openRecentList();
	void saveAsList(QString* = 0);
	void saveList();
	void printList();
	void closeList();
	void lista1Changed();
	void lista2Changed();
	void generujListe();
	void generujSlowka();
	void font1( QFont* = 0 );
	void font2( QFont* = 0 );
	void kolor1();
	void kolor2();
	void zamknij();
	void defaults();
	void apply();
	void truncate();
	void switchLists();
	void showStats();
        void synth();
        void synthCompleted();

    void popupAccepted(QString w, QStringList t);

private:
        /* Add word from clipboard to application. If add is true word will
         * be registered on list etc., if popup is configured it will
         * be displayed. */
        void _newWord(bool add);

        Core *core;
        Words *words;
        Ui::Slowka* ui;
	Mini *mini;
	Stats *stats;

        QStringList slowa1, slowa2;

        SoundGenerator *generator;
	QClipboard *clipboard;
        QString recentFileName, fileName;
        QSystemTrayIcon *tray;
};

#endif
