#ifndef PARLEYINTERFACE_H
#define PARLEYINTERFACE_H

#include <QObject>
#include <QStringList>

class ParleyInterface : public QObject
{
    Q_OBJECT
public:
    explicit ParleyInterface(QObject *parent = 0);

signals:

public slots:
    void add(QString, QStringList);

private:
    QString cachedName;

    QString findParleyProcName();
};

#endif // PARLEYINTERFACE_H
