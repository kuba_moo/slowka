#include "application.h"

#include <QDebug>
#include <QX11Info>

extern "C" {
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
}

/**** Begining of xbindkeys code.
 * This code have been stolen from xbindkeys 1.8.5.
 */
static unsigned int numlock_mask = 0;
static unsigned int scrolllock_mask = 0;
static unsigned int capslock_mask = 0;

static void
get_offending_modifiers (Display * dpy)
{
  int i;
  XModifierKeymap *modmap;
  KeyCode nlock, slock;
  static int mask_table[8] = {
    ShiftMask, LockMask, ControlMask, Mod1Mask,
    Mod2Mask, Mod3Mask, Mod4Mask, Mod5Mask
  };

  nlock = XKeysymToKeycode (dpy, XK_Num_Lock);
  slock = XKeysymToKeycode (dpy, XK_Scroll_Lock);

  /*
   * Find out the masks for the NumLock and ScrollLock modifiers,
   * so that we can bind the grabs for when they are enabled too.
   */
  modmap = XGetModifierMapping (dpy);

  if (modmap != NULL && modmap->max_keypermod > 0)
    {
      for (i = 0; i < 8 * modmap->max_keypermod; i++)
        {
          if (modmap->modifiermap[i] == nlock && nlock != 0)
            numlock_mask = mask_table[i / modmap->max_keypermod];
          else if (modmap->modifiermap[i] == slock && slock != 0)
            scrolllock_mask = mask_table[i / modmap->max_keypermod];
        }
    }

  capslock_mask = LockMask;

  if (modmap)
    XFreeModifiermap (modmap);
}

static void
my_grab_key (Display * dpy, KeyCode keycode, unsigned int modifier,
             Window win)
{
  modifier &= ~(numlock_mask | capslock_mask | scrolllock_mask);


  XGrabKey (dpy, keycode, modifier, (win ? win : DefaultRootWindow (dpy)),
            False, GrabModeAsync, GrabModeAsync);

  if (modifier == AnyModifier)
    return;

  if (numlock_mask)
    XGrabKey (dpy, keycode, modifier | numlock_mask,
              (win ? win : DefaultRootWindow (dpy)),
              False, GrabModeAsync, GrabModeAsync);

  if (capslock_mask)
    XGrabKey (dpy, keycode, modifier | capslock_mask,
              (win ? win : DefaultRootWindow (dpy)),
              False, GrabModeAsync, GrabModeAsync);

  if (scrolllock_mask)
    XGrabKey (dpy, keycode, modifier | scrolllock_mask,
              (win ? win : DefaultRootWindow (dpy)),
              False, GrabModeAsync, GrabModeAsync);

  if (numlock_mask && capslock_mask)
    XGrabKey (dpy, keycode, modifier | numlock_mask | capslock_mask,
              (win ? win : DefaultRootWindow (dpy)),
              False, GrabModeAsync, GrabModeAsync);

  if (numlock_mask && scrolllock_mask)
    XGrabKey (dpy, keycode, modifier | numlock_mask | scrolllock_mask,
              (win ? win : DefaultRootWindow (dpy)),
              False, GrabModeAsync, GrabModeAsync);

  if (capslock_mask && scrolllock_mask)
    XGrabKey (dpy, keycode, modifier | capslock_mask | scrolllock_mask,
              (win ? win : DefaultRootWindow (dpy)),
              False, GrabModeAsync, GrabModeAsync);

  if (numlock_mask && capslock_mask && scrolllock_mask)
    XGrabKey (dpy, keycode,
              modifier | numlock_mask | capslock_mask | scrolllock_mask,
              (win ? win : DefaultRootWindow (dpy)), False, GrabModeAsync,
              GrabModeAsync);

}
/**** End of xbindkeys code. */


Application::Application(int& argc, char** argv) :
    QApplication(argc, argv)
{
    get_offending_modifiers(QX11Info::display());
}

bool Application::x11EventFilter(XEvent *ev)
{
    /* No active window and keypress?? Probably a global shortcut... */
    if (!activeWindow() && ev->type == KeyPress) {
        unsigned int Xmodifiers = ev->xkey.state;
        QString sequence;

        Xmodifiers &= ~(numlock_mask | capslock_mask | scrolllock_mask);

        if (! Xmodifiers)
            return QApplication::x11EventFilter(ev);
        if (Xmodifiers & ControlMask)
            sequence += "Ctrl+";
        if (Xmodifiers & Mod1Mask)
            sequence += "Alt+";
        if (Xmodifiers & Mod2Mask)
            sequence += "Meta+";
        if (Xmodifiers & ShiftMask)
            sequence += "Shift+";

        sequence += QString((char)XKeycodeToKeysym(QX11Info::display(),
                                                   ev->xkey.keycode, 0)).toUpper();

        qDebug() << "Received globally" << QKeySequence(sequence);
        QAction* action = globalShortcuts[QKeySequence(sequence)];
        if (action)
            action->trigger();
    }

    return QApplication::x11EventFilter(ev);
}

void Application::registerGlobalShortcut(QAction *action)
{
    qDebug() << "register" << action->shortcut() << "as global shortcut";

    registerGlobalShortcut(action, action->shortcut());
}

void Application::registerGlobalShortcut(QAction *action, QKeySequence shortcut)
{
    quint32 Xmodifiers = 0, Xkey = 0;

    globalShortcuts[shortcut] = action;
    QString key;
    QStringList keys = shortcut.toString().split('+');
    foreach (key, keys) {
        if (key.toLower() == "ctrl")
            Xmodifiers |= ControlMask;
        else if (key.toLower() == "alt")
            Xmodifiers |= Mod1Mask;
        else if (key.toLower() == "meta")
            Xmodifiers |= Mod2Mask;
        else if (key.toLower() == "shift")
            Xmodifiers |= ShiftMask;
        else if (key.length() == 1)
            Xkey = key[0].toAscii();
        else
            qDebug() << __PRETTY_FUNCTION__ << "Unrecognized key:" << key;
    }

    if (Xmodifiers && Xkey)
        my_grab_key(QX11Info::display(),
                    XKeysymToKeycode(QX11Info::display(), Xkey),
                    Xmodifiers, 0);
}
