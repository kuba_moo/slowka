#include <QtGui>

#include "mini.h"
#include "slowka.h"

Mini::Mini(QStringList *one, QStringList *two, QWidget *parent)	: QWidget(parent, Qt::ToolTip | Qt::WindowStaysOnTopHint) {
	slo1 = one;
	slo2 = two;
	pressed = false;
	jeden.setColor(QPalette::Text, QColor(155,0,0));
	dwa.setColor(QPalette::Text, QColor(0,155,0));
	this->setGeometry(300,130,440,29);
        this->setMaximumWidth(2000);
	this->setWindowTitle("Mini");
	this->setWindowIcon(QIcon(":/mini"));
	delBut = new QPushButton(QIcon(":/minus"), QString(), this);
	delBut->setFlat(true);
	pauseBut= new QPushButton(QIcon(":/pause"), QString(), this);
	pauseBut->setFlat(true);
	pauseBut->setCheckable(true);
	newBut= new QPushButton(QIcon(":/plus"), QString(), this); 
	newBut->setFlat(true);

	QFont font("Sans Serif", 12);	
	for(int i=0; i<15; i++ ) {
		label[0][i] = new QLabel(this);
		label[1][i] = new QLabel(this);
		label[0][i]->setFont(font);
		label[1][i]->setFont(font);
		label[0][i]->setVisible(false);
		label[1][i]->setVisible(false);
		label[0][i]->setPalette(jeden);
		label[1][i]->setPalette(dwa);
		label[0][i]->setForegroundRole(QPalette::Text);
		label[1][i]->setForegroundRole(QPalette::Text);
	}
}

Mini::~Mini(){
}

void Mini::kolor1() {
	for(int i=0; i<15; i++ )
		label[0][i]->setPalette(jeden);
	
	uloz();	
}

void Mini::kolor2() {
	for(int i=0; i<15; i++ )
		label[1][i]->setPalette(dwa);
	
	uloz();	
}

void Mini::font1( QFont* font ) {
	for(int i=0; i<15; i++ )
		label[0][i]->setFont(*font);
	
	uloz();
}

void Mini::font2( QFont* font ) {
	for(int i=0; i<15; i++ )
		label[1][i]->setFont(*font);
	
	uloz();
}

void Mini::uloz() {
	int start=5, i=0, last;
	last = (slo1->size() > slo2->size()) ? slo1->size()-1 : slo2->size()-1;
	
	while( ( last>=0 ) && ( i<15 ) && (start < this->width()-90) ) {
		if( slo1->size() > last ) {
			label[0][i]->setText(slo1->at(last).trimmed());
			label[0][i]->setVisible(true); 
			label[0][i]->setGeometry(start,7,10,10);
			label[0][i]->adjustSize();
			start+=label[0][i]->width()+5;
		} else
			label[0][i]->setVisible(false);
		if( (slo2->size() > last) && (start < this->width()-90) ) {
			label[1][i]->setText(slo2->at(last).trimmed());
			label[1][i]->setVisible(true); 
			label[1][i]->setGeometry(start,7,10,10);
			label[1][i]->adjustSize();
			start+=label[1][i]->width()+5;
		} else
			label[1][i]->setVisible(false);
		last--;
		i++;
	}
	if( i ) {
		if( label[1][i-1]->isVisible() )
			label[1][i-1]->setVisible(false);
		else
			label[0][i-1]->setVisible(false);
			label[1][i-1]->setVisible(false);
	}
	
	newBut->setGeometry(this->width()-30,3,27,23);
	pauseBut->setGeometry(this->width()-60,3,27,23);
	delBut->setGeometry(this->width()-90,3,27,23);	
	
	while( i<15 ) {
		label[0][i]->setVisible(false);
		label[1][i]->setVisible(false);
		i++;
            }
}

void Mini::mouseMoveEvent(QMouseEvent *event) {
	if ((event->buttons() & Qt::LeftButton) && pressed)
		this->move(this->pos()+event->pos()-start);
}

void Mini::mousePressEvent(QMouseEvent *event){
	start = event->pos();
	pressed = true;
}

void Mini::mouseReleaseEvent(QMouseEvent *event){
	pressed = false;
	event->accept();
}

void Mini::wheelEvent(QWheelEvent * event) {
	int delta = event->delta()/2;
        if(delta+this->width()>100 && delta+this->width()<2000)
		this->setGeometry(this->x()-delta/2,this->y(),this->width()+delta,this->height());
	uloz();
	event->accept();
}

void Mini::closeEvent(QCloseEvent *event) {
	emit zamknieto();
	event->accept();
}
