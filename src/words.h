#ifndef WORDS_H
#define WORDS_H

#include <QObject>
#include <QList>
#include <QSet>
#include <QString>
#include <QStringList>

/** Single word.
  *
  * Object represents word and it's translations.
  */
class Word {
public:
    QString word;
    QStringList translations;
};

/** Words store.
  *
  * This class is responsible for storing words and translations.
  */
class Words : public QObject
{
    Q_OBJECT
public:
    enum InsertionStatus {
        Added,       /* Success */
        Duplicate,   /* Not added - new word is duplicate */
        NoEntry      /* There is no word to add translation to */
    };

    explicit Words(QObject *parent = 0);
    ~Words();

    /* Appends new word into database. */
    InsertionStatus addWord(QString w);
    /* Appends new translation to last word. */
    InsertionStatus addTranslation(QString t);

    /* Remove all words */
    void clear();

    /* Get precompiled lists of words */
    const QStringList& getWords();
    const QStringList& getTranslations();

signals:
    void wordsChanged();  /* Change to words happend. */
    void translationsChanged();  /* Change to translations happend. */

public slots:
    /* Appends both word and the translation, makes sure they correlate. */
    InsertionStatus add(QString w, QStringList t);
    /* Appends new entity translation if last word lacks one or word. */
    InsertionStatus append(QString s);
    /* Removes last entity added translation or word. */
    void removeLast();

private:
    QList<Word *> entries;
    QSet<QString> wordsSet;

    QStringList wordsList, translationsList;
};

#endif // WORDS_H
