#include "ivonaloader.h"

#include <QDebug>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFile>
#include <QThread>

IvonaLoader::IvonaLoader(int num_managers, QObject *parent) :
    QObject(parent), numManagers(num_managers)
{
    for (int i=0; i < numManagers; i++)
    {
        qDebug() << "IvonaLoader: creating accessManager no." << i;

        spawnAccessManager();
    }

    connect(this, SIGNAL(workAdded()), SLOT(tryToWork()));
}

void IvonaLoader::spawnAccessManager()
{
    QNetworkAccessManager* accessManager = new QNetworkAccessManager(this);
    ManagerDesc desc;
    desc.startRequest(ManagerDesc::NotConnected);
    managersDesc.insert(accessManager, desc);

    managersWorking.append(accessManager);

    connect(accessManager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(initializeManager(QNetworkReply*)));

    QNetworkRequest request(QUrl("http://www.ivona.com/#"));

    accessManager->get(request);
}

void IvonaLoader::enqueueSynth(SampleDesc *desc)
{
    qDebug() << "IvonaLoader: adding work" << desc->text;

    workLock.lock();
    work.enqueue(desc);
    workLock.unlock();

    emit workAdded();
}

QPair<SampleDesc*, QByteArray> IvonaLoader::getResult()
{
    QPair<SampleDesc*, QByteArray> ret = QPair<SampleDesc*, QByteArray>(0,"");

    resultsLock.lock();
    if (! results.isEmpty())
        ret = results.dequeue();
    resultsLock.unlock();

    return ret;
}

void IvonaLoader::tryToWork()
{
    while (! managersReady.empty())
    {
        workLock.lock();
        if (work.isEmpty())
        {
            workLock.unlock();
            return;
        }
        SampleDesc *sample = work.dequeue();
        workLock.unlock();
        qDebug() << "IvonaLoader: starting work" << sample->text;

        QNetworkAccessManager* accessManager = managersReady.dequeue();
        managersDesc[accessManager].startRequest(ManagerDesc::GeneratingLink);
        managersDesc[accessManager].sample = sample;

        QString postData = QString("tresc=%1")
                .arg(QString(QUrl::toPercentEncoding(sample->text))) +
                "&flashdet=&synth=1&chk=V2l0YWo.&token="
                + QString("&voice=%1").arg(sample->voice)
                + QString("&lang=%1").arg(sample->lang) +
                "&io_voice=&io_text=&tokenUrl=http%3A%2F%2Fwww.ivona.com%2F%3Ftk%3D";

        connect(accessManager, SIGNAL(finished(QNetworkReply*)),
                SLOT(generateLink(QNetworkReply*)));

        QNetworkRequest request(QUrl("http://www.ivona.com/#"));
        accessManager->post(request, postData.toAscii());

        managersWorking.append(accessManager);
    }
}

void IvonaLoader::initializeManager(QNetworkReply *reply)
{
    QNetworkAccessManager* accessManager = reply->manager();

    qDebug() << "IvonaLoader: manager ready";

    accessManager->disconnect(this, SLOT(initializeManager(QNetworkReply*)));

    managersDesc[accessManager].state = ManagerDesc::SessionReady;

    managersWorking.removeOne(accessManager);
    managersReady.enqueue(accessManager);

    tryToWork();
}

void IvonaLoader::generateLink(QNetworkReply *reply)
{
    QByteArray line;
    reply->manager()->disconnect(this, SLOT(generateLink(QNetworkReply*)));

    while (reply->canReadLine())
    {
        line = reply->readLine();

        if (line.contains(";loadAudioFile"))
        {
            line.remove(0, line.indexOf(";loadAudioFile")+16);
            line.remove(line.indexOf("'"), 1000);

            QNetworkRequest request(QUrl(line.constData()));
            managersDesc[reply->manager()].startRequest(ManagerDesc::DownloadingMp3);
            reply->manager()->get(request);
            connect(reply->manager(), SIGNAL(finished(QNetworkReply*)),
                    SLOT(downloadMp3(QNetworkReply*)));
            break;
        }
    }

    qDebug() << "IvonaLoader: link generated, starting downloading mp3";
}

void IvonaLoader::downloadMp3(QNetworkReply *reply)
{
    QNetworkAccessManager *manager = reply->manager();
    bool success = reply->rawHeader("Content-Type").contains("mpeg");

    /* Do we have to retry? */
    if (! success && managersDesc[manager].retryCount++ < ManagerDesc::maxRetry)
    {
        manager->get(reply->request());
        qDebug() << "IvonaLoader: retrying on " << managersDesc[manager].sample->text;
        return;
    }

    /* Finishing with this one either because it was downloaded or download failed
     * and retry count has been exceeded. */
    managersWorking.removeOne(manager);
    disconnect(manager, SIGNAL(finished(QNetworkReply*)),
               this, SLOT(downloadMp3(QNetworkReply*)));

    if (success)
    {
        resultsLock.lock();
        results.enqueue(QPair<SampleDesc*, QByteArray>
                        (managersDesc[manager].sample, reply->readAll()));
        resultsLock.unlock();
        qDebug() << "IvonaLoader: download completed"
                 << managersDesc[manager].sample->text;
        emit downloadCompleted();
    }
    else
    {
        qDebug() << "IvonaLoader: download failed"
                 << managersDesc[manager].sample->text;
        emit downloadFailed();
    }

    if (managersDesc[manager].useCount > ManagerDesc::maxRequests)
    {
        managersDesc.remove(manager);
        manager->deleteLater();
        qDebug() << "IvonaLoader: recycling manager";
        spawnAccessManager();
        return;
    } else {
        managersDesc[manager].state = ManagerDesc::SessionReady;
        managersReady.append(manager);
    }

    tryToWork();
}
